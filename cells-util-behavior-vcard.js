const propNotAvalaiblesSendRequest = ['label', 'value', 'isKeyValue', 'root', 'required', 'addSessionHeaders', 'keySessionHeaders', 'addAll', 'textAddAll']
window.CellsBehaviors = window.CellsBehaviors || {};
CellsBehaviors.cellsUtilBehaviorVcard = (superClass) => class extends superClass {
  
  applyGenerics() {
    this.genericDm = document.getElementById('dmGeneric');
    this.genericAccessControl = document.getElementById('controlAccessDmGeneric');
  }

  constructor() {
    super();
    this.notificaciones = [];
    this.applyGenerics();
    if (this.events && this.navigate) {

      window.addEventListener(this.events.optionMenuSelected, (event)=>{
        console.log('Menu selected', event);
        let valueOption = this.extract(event.detail, 'valor', null);
        if(valueOption){
            let menu = valueOption.split('#');
            try {
              this.navigate(menu[0]);
            } catch (error) {
              console.log('Pagina no encontrada', error);
            }
        }
      });

      window.addEventListener(this.events.logOut, (event) => {
        console.log('LogOut', event);
        window.sessionStorage.setItem(this.ctts.keys.userSesion,null);
        window.sessionStorage.removeItem(this.ctts.keys.userSesion);
        window.location = this.ctts.contextPath;
      });

      window.addEventListener(this.events.logoSelected, (event) => {
        this.navigate('main');
      });

      window.addEventListener(this.events.goUserProfile, (event) => {
        this.navigate('profile');
      });

      window.addEventListener(this.events.globalSpinner, async(event) => {
        if (document.querySelector('cells-spinner-vcard')) {
          document.querySelector('cells-spinner-vcard').activeLoading = await event.detail;
        }
      });
    }
  }

  alertMsg(options) {
    document.getElementById('messageAlert').add(options);
  }

  visibleHeader(display) {
    if(document.querySelector('cells-ui-header-vcard')) {
      document.querySelector('cells-ui-header-vcard').visible = display;
    }
  }

  setUserHeader(user) {
    if(document.querySelector('cells-ui-header-vcard')) {
      document.querySelector('cells-ui-header-vcard').setUser(user);
    }
  }

  onScketNotificationInit(callback, dm) {

    console.log('window.AppConfig.socketVcard', window.AppConfig.socketVcard);
    window.AppConfig.socketVcard.on('messageToClient', (bodyMessage) => {
      console.log('bodyMessage', bodyMessage);
      if (callback) {
        callback();
      }
    });
    this.addEventListener(this.events.removeNotification, (event) => {
      console.log('closeNotification', event.detail)
      if (event.detail && event.detail._id) {
        let settings = {};
        settings.method = 'PUT';
        settings.onSuccess = (response) => {
          if (callback) {
            callback();
          }
        }
        settings.path = `${this.services.endPoints.notificacionesCheck}/${event.detail._id}`;
        this.dmEjecutor.sendRequest(settings);
      }
    });
  }

  cargarNotificaciones(dm, onSuccess) {
    if (this.userSession.oficina) {
      let settings = {};
      if (onSuccess) {
        settings.onSuccess = onSuccess;
      } else {
        settings.onSuccess = (response) => {
          this.notificaciones = response.detail;
        }
      }
      settings.path = `${this.services.endPoints.notificacionesList}/${this.userSession.oficina._id}`;
      dm.sendRequest(settings);
    }
  }

  validateSessionRoute() {
    if(this.navigate && window.location.href.indexOf('#!/') >= 0) {
      const pathRoute = window.location.href.substring((window.location.href.indexOf('#!/')+3));
      if(pathRoute && !window.sessionStorage.getItem(this.ctts.keys.userSesion)) {
        window.location = '/';
      }
    }
  }

  static get properties() {
    return {
      services: {
        type: Object
      },
      ctts: {
        type: Object
      },
      events: {
        type: Object
      },
      userSession: {
        type: Object
      },
      tokenBearer: {
        type: String
      },
      notificaciones: {
        type: Array
      },
      dmEjecutor: {
        type: Object
      },
      idDm: {
        type: String
      },
      genericDm: {
        type: Object
      },
      genericAccessControl: {
        type: Object
      }
    };
  }


  get ctts() {
    return this.extract(window, 'AppConfig.ctts', {});
  }

  get events() {
    return this.extract(window, 'AppConfig.events', {});
  }

  get services() {
    return this.extract(window, 'AppConfig.services', {});
  }

  get userSession() {
    return this.getUserSesion()
  }

  get tokenBearer() {
    let tokenAuth = window.sessionStorage.getItem(this.ctts.keys.token);
    return `Berear ${tokenAuth}`;
  }
  
  formatDate(date){
    try{
      date = new Date(date);
      let dateStr =
      ('00' + date.getDate()).slice(-2) + '/' +
      ('00' + (date.getMonth() + 1)).slice(-2) + '/' +
      date.getFullYear() + ' ' +
      ('00' + date.getHours()).slice(-2) + ':' +
      ('00' + date.getMinutes()).slice(-2) ;
    return dateStr;
    }catch(e){
      return '';
    }
    
  }

  getById(id) {
    return this.shadowRoot.querySelector(`#${id}`);
  }

  getInputValue(id) {
    let input = this.getById(id);
    return (input ? input.value : undefined);
  }

  getSelectedIndex(id, lista) {
    let cmb = this.getById(id);
    return (cmb && cmb.selected !== undefined ? lista[cmb.selected] : undefined);
  }

  dispatch(name, detail) {
    const val = typeof detail === 'undefined' ? null : detail;
    this.dispatchEvent(new CustomEvent(name, {
      composed: true,
      bubbles: true,
      detail: val
    }));
  }

  extract(data, keys, value) {
    let ret;
    if (!this.isEmpty(data) && !this.isEmpty(keys)) {
      let split = keys.split('.');
      ret = data[split.shift()];
      while (ret && split.length) {
        ret = ret[split.shift()];
      }
    }
    return this.isEmpty(ret) && value !== null ? value : ret;
  }

  isEmpty(evaluate) {
    return this._isEmpty(evaluate);
  }

  _isEmpty(evaluate) {
    switch (typeof (evaluate)) {
      case 'object':
        return evaluate === null || Object.keys(evaluate).length === 0;
      case 'string':
        return evaluate === '';
      case 'undefined':
        return true;
      default:
        return false;
    }
  }

  getUserSesion() {
   let userSesion = window.sessionStorage.getItem(this.ctts.keys.userSesion);
   if(typeof userSesion === 'string') {
      userSesion = JSON.parse(userSesion);
   }
   return userSesion;
  }

  adaptStringHtml(stringHtml) {
    var t = document.createElement('template');
    t.innerHTML = stringHtml;
    return t.content.cloneNode(true);
  }

  getNextPage(pagination) {
    if (this.isEmpty(pagination)) {
      return '';
    } else {
      let nextPage = this.extract(pagination, 'nextPage');
      if (!this.isEmpty(nextPage)) {
        return nextPage;
      }
      nextPage = this.extract(pagination, 'links.next');
      if (!this.isEmpty(nextPage)) {
        return nextPage;
      }
    }
    return '';
  }

   /**
   * method that transforms an array of objects into another one of key value structure
   * @method parseResponseKeyValueCmb
   * @param {Object}  response
   * @param {String}  label
   * @param {String}  value
   * @param {String}  root
   * @param {String}  labelCustom
   */
  parseResponseKeyValueCmb(response, label, value, root, labelCustom) {
    let data;
    //console.log('cellsBaseBehaviorPfco.parseResponseKeyValueCmb',response)
    if (root) {
      if (response[root]) {
        data = response[root];
      } else {
        data = [];
        data.push(response);
      }
    } else {
      if (Array.isArray(response)) {
        data = response;
      } else {
        data = [];
        data.push(response);
      }
    }
    let reformattedArray = [];
    if (data && data.map) {
      reformattedArray = data.map((record) => {
        let labelValue = {};
        if (labelCustom) {
          labelValue.label = this.labelCustomFormated(labelCustom, record);
        } else {
          labelValue.label = record[label];
        }
        if (value.indexOf('.') > 0) {
          labelValue.value = this.extract(record, value, '');
        } else {
          labelValue.value = record[value];
        }
        labelValue.record = record;
        return labelValue;
      });
    }
    return reformattedArray;

  }

  /**
 * Method that obtains the properties of a recursive object
 * @method listPropertiesObject
 * @param {Object}   record
 */
  listPropertiesObject(record) {
    const isObject = val => typeof val === 'object' && !Array.isArray(val);
    const paths = (obj = {}) =>
      Object.entries(obj)
        .reduce(
          (product, [key, value]) =>
            isObject(value) ?
              product.concat([
                [key, paths(value)]
              ]) :
              product.concat([key]),
          []
        );
    const addDelimiter = (a, b) =>
      a ? `${a}.${b}` : b;
    const pathToString = ([root, children]) =>
      children.map(
        child =>
          Array.isArray(child) ?
            addDelimiter(root, pathToString(child)) :
            addDelimiter(root, child)
      )
        .join(':::');
    return pathToString(['', paths(record)]).split(':::');
  }

  /**
  * method that executes the loading of the combos of a component
  * @method labelCustomFormated
  * @param {String}   labelCustom
  * @param {Object}   record
  */
  labelCustomFormated(labelCustom, record) {
    let propiedades = this.listPropertiesObject(record) || [];
    propiedades.forEach((property) => {
      let find = '{' + property + '}';
      let value;
      if (property.indexOf('.') > 0) {
        value = this.extract(record, property, '');
      } else {
        value = record[property];
      }
      let re = new RegExp(find, 'g');
      labelCustom = labelCustom.replace(re, value);
    });
    return labelCustom;
  }

  /**
  * method that executes the loading of the combos of a component
  * @method inputSelectLoadData
  * @param {String}   eventName
  * @param {Object}   thisParent
  */
  inputSelectLoadData(eventName, thisParent) {
    const selects = thisParent.shadowRoot.querySelectorAll('cells-select');
    if (selects && selects.length > 0) {
      selects.forEach((select) => {
        let options = select.dataset || {};
        if (options.initialLoad !== 'false') {
          this.loadDataCmb(select, eventName, thisParent);
        }
      });
    }
  }

  /**
   * Method that invokes data loading for combo
   * @method loadDataCmb
   * @param {Object} select
   * @param {String} eventName
   * @param {Object} thisParent
   * @param {String} path
   */
  loadDataCmb(select, eventName, thisParent, path) {
    let options = select.dataset || {};
    if (!this.isEmpty(options)) {
      if (path) {
        options.path = path;
      }
      let config = {};
      for (let option in options) {
        if (options.hasOwnProperty(option) &&
          !propNotAvalaiblesSendRequest.includes(option)) {
          config[option] = options[option];
        }
      }
      if (!config.host) {
        config.host = this.services.host;
      }
      config.selectElement = select;

      config.onSuccess = (xhr) => {
        if (typeof options.isKeyValue !== 'undefined') {
          if (xhr.detail && !xhr.response) {
            xhr.response = xhr.detail;
          }
          if(typeof options.addAll !== 'undefined') {
            let addTemp = [ {label: (options.textAddAll ? options.textAddAll: 'Todos' ) , value: 'empty-value', record:{}} ];
            let finalTemp = addTemp.concat(this.parseResponseKeyValueCmb(xhr.response, options.label, options.value, options.root, options.labelCustom));
            select.options = finalTemp; 
          }else{
            select.options = this.parseResponseKeyValueCmb(xhr.response, options.label, options.value, options.root, options.labelCustom);
          }
          
        } else {
          if(typeof options.addAll !== 'undefined') {
            let addTemp = [ {label: (options.textAddAll ? options.textAddAll: 'Todos' ) , value: 'empty-value', record:{}} ];
            let finalTemp = addTemp.concat((options.root ? xhr.response[options.root] : xhr.response));
            select.options = finalTemp; 
          }else{
            select.options = (options.root ? xhr.response[options.root] : xhr.response);
          }
        }
        thisParent.requestUpdate();
        select.dispatchEvent(new CustomEvent(this.events.loadDataCellsSelectComplete, { composed: true, bubbles: true, detail: {response: xhr.response, select:select } }));
      }
      
      this.dispatch(eventName, config);
    }
  }

  /**
   * Correction of the @selected-option-changed event of the cells-select component
   * @method selectedOptionChanged
   * @param {Event} e
   * @param {Function} callBack
   */
  selectedOptionChanged(e, callBack) {
    const { detail } = e;
    const select = e.path[0];
    let dataset = select.dataset || {};
    if (detail && detail.value && detail.value.value && select) {
      let newValue = detail.value.value;
      if (dataset.valueSelectedEvent !== newValue) {
        dataset.valueSelectedEvent = newValue;
        callBack(e);
      }
    }
  }

  /**
     * Correction of the @selected-changed event of the cells-select component
     * @method selectedChanged
     * @param {Event} e
     * @param {Function} callBack
     */
  selectedChanged(e, callBack) {
    const { detail } = e;
    const select = e.path[0];
    let dataset = select.dataset || {};
    if (detail && detail.value && select) {
      let newValue = detail.value;
      if (dataset.valueSelectedEvent !== newValue) {
        dataset.valueSelectedEvent = newValue;
        callBack(e);
      }
    }
  }
  /**
       * Method that validates the inputs of a form
       * @method validateForm
       * @param {String} selector
       */
  validateForm(selector) {
    let inputs = this.shadowRoot.querySelectorAll(selector || 'bbva-input-field, cells-select, cells-money-input-pfco');
    console.log(inputs);
    if (!inputs) {
      return true;
    }
    let countErrors = 0;
    inputs.forEach((input) => {
  
      if ((input.required || (input.dataset && input.dataset.required)) && this.isEmpty(input.value)) {
        if (input.tagName.toUpperCase() === 'CELLS-SELECT') {
          input.error = true;
        }else {
          input.invalid = true;
        }
        countErrors++;
      }
      let emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
      if (input.type === 'email' &&
        !(emailRegex.test(input.value))) {
        input.invalid = true;
        countErrors++;
      }
      if (input.type === 'date') {
        //TODO por agregar la validacion de fecha date
        /*
        let format = 'YYYY-MM-DD';
        if (input.dataset && input.dataset.format) {
          format = input.dataset.format;
        }
        let fecha = momentJs(input.value, format);
        if (!(momentJs.isDate(fecha._d))) {
          input.invalid = true;
          countErrors++;
        }
        */
      }
      if (input.dataset && (input.dataset.documentType === 'DNI' || input.dataset.documentType === 'RUC')) {
        if (!Number.isInteger(parseInt(input.value))) {
          input.invalid = true;
          countErrors++;
        } else {
          if (input.value.indexOf('.') >= 0) {
            input.invalid = true;
            countErrors++;
          } else {
            switch (input.dataset.documentType) {
              case 'DNI':
                input.maxLength = 8;
                if (input.value.length !== 8) {
                  input.invalid = true;
                  countErrors++;
                }
                break;
              case 'RUC':
                input.maxLength = 11;
                if (input.value.length !== 11) {
                  input.invalid = true;
                  countErrors++;
                }
                break;
              default:
                break;
            }
          }
        }
      }
    });
    this.requestUpdate();
    return (countErrors === 0);
  }

  loadDmParameters(dm, path, onSuccess, onError) {
    if(!onSuccess){
      onSuccess = ()=>{};
    }
    if(!onError){
      onError = ()=>{};
    }
    let settings;
    settings = {
      path: path,
      onSuccess: onSuccess,
      onError: onError
    };
    dm.sendRequest(settings);
  }

};